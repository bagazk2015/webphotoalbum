﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Services;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Interfaces;

namespace WebPhotoAlbum.Test
{
    [TestClass]
    public class TagServiceTest
    {
        Mock<IUnitOfWork> mockUOW;
        Mock<IGenericRepository<Tag>> mockTagRepository;


        public void Initialize()
        {
            mockUOW = new Mock<IUnitOfWork>();

            mockTagRepository = new Mock<IGenericRepository<Tag>>();
            mockTagRepository.Setup(m => m.FindById(1)).Returns(new Tag() { Id = 1, Title = "tag1" });
            mockTagRepository.Setup(m => m.FindById(2)).Returns(new Tag() { Id = 2, Title = "tag2" });
            mockTagRepository.Setup(m => m.FindById(3)).Returns(new Tag() { Id = 3, Title = "tag3" });
            mockTagRepository.Setup(m => m.Get(It.IsAny<Func<Tag, bool>>())).Returns(GetTestTags());
            

        }
        private List<Tag> GetTestTags()
        {
            var tags = new List<Tag>
            {
               new Tag() { Id = 1, Title ="tag1"},
               new Tag() { Id = 2, Title = "tag2" },
               new Tag() { Id = 3, Title = "tag3" }
            };
            return tags;
        }

        

    }

}
