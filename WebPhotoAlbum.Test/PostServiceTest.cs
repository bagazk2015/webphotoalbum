﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Services;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Interfaces;

namespace WebPhotoAlbum.Test
{
    [TestClass]
   public class PostServiceTest
    {
        Mock<IUnitOfWork> mockUOW;
        Mock<IGenericRepository<Post>> mockPostRepository;
        Mock<IGenericRepository<Tag>> mockTagRepository;

        public void Initialize()
        {
            mockUOW = new Mock<IUnitOfWork>();
            
            mockPostRepository = new Mock<IGenericRepository<Post>>();
            mockPostRepository.Setup(m => m.FindById(1)).Returns(new Post() { Id = 1, AuthorId = 1, Description = "desc1" });
            mockPostRepository.Setup(m => m.FindById(2)).Returns(new Post() { Id = 2, AuthorId = 1, Description = "desc2" });
            mockPostRepository.Setup(m => m.FindById(3)).Returns(new Post() { Id = 3, AuthorId = 1, Description = "desc3" });
            mockPostRepository.Setup(m => m.Get()).Returns(GetTestPosts());
            mockPostRepository.Setup(m => m.GetWithInclude(It.IsAny <Func<Post, bool>>(), It.IsAny<Expression<Func<Post, object>>[]>())).Returns(GetTestPosts());
            mockTagRepository = new Mock<IGenericRepository<Tag>>();
            mockTagRepository.Setup(m => m.FindById(1)).Returns(new Tag() { Id = 1, Title = "tag1" });
            mockTagRepository.Setup(m => m.FindById(2)).Returns(new Tag() { Id = 2, Title = "tag2" });
            mockTagRepository.Setup(m => m.FindById(3)).Returns(new Tag() { Id = 3, Title = "tag3" });
            mockTagRepository.Setup(m => m.Get()).Returns(GetTestTags());
            PostService postService = new PostService(mockUOW.Object, new TagService(mockUOW.Object));

            mockUOW.Setup(m => m.Posts).Returns(mockPostRepository.Object);


        }
        private List<Post> GetTestPosts()
        {

            var posts = new List<Post>
            {
            new Post() { Id=1, AuthorId=1,Description="desc1" },
            new Post() { Id = 2, AuthorId = 1, Description = "desc2" },
            new Post() { Id=3, AuthorId=1,Description="desc3" }
            };
            return posts;
        }
        private List<Tag> GetTestTags()
        {
            var tags = new List<Tag>
            {
               new Tag() { Id = 1, Title ="tag1"},
               new Tag() { Id = 2, Title = "tag2" },
               new Tag() { Id = 3, Title = "tag3" }
            };
            return tags;
        }
        [TestMethod]
        public void CanFindPostById()
        {
            Initialize();
            PostService postService = new PostService(mockUOW.Object, new TagService(mockUOW.Object));

            PostDTO postDTO = postService.FindById(1);

            Assert.AreEqual(postDTO.Id, 1);

        }
        //[TestMethod]
        //public void CanGetPostsWithSkip()
        //{
        //    Initialize();
        //    PostService postService = new PostService(mockUOW.Object, new TagService(mockUOW.Object));

        //    List<PostDTO> postDTOs = postService.GetPostsWithSkip(1, 1, "");

        //    Assert.AreEqual(postDTOs.Count, 1);


        //}


    }
}
