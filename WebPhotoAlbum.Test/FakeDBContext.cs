﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.DAL;
using WebPhotoAlbum.DAL.Entities;

namespace WebPhotoAlbum.Test
{
   public class FakeDBContext : IdentityDbContext<ApplicationUser, CustomRole,
       int, CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        public FakeDBContext() : base("TestConnection")
        {
            Database.SetInitializer<EFDbContext>(new DropCreateDatabaseIfModelChanges<EFDbContext>());
            //Database.SetInitializer<EFDbContext>(new IdentityDbInit());
        }

        public DbSet<Admin> Admins { get; set; }


        public DbSet<Author> Authors { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Rating> Ratings { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public static EFDbContext Create()
        {
            return new EFDbContext();
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {


            modelBuilder.Entity<ApplicationUser>()
                  .HasOptional(c => c.Author)
                  .WithRequired(d => d.ApplicationUser).WillCascadeOnDelete(false);


            modelBuilder.Entity<ApplicationUser>()
                .HasOptional(c => c.Admin)
                .WithRequired(d => d.ApplicationUser);



            modelBuilder.Entity<Author>()
           .HasMany(c => c.Posts)
           .WithRequired(o => o.Author)
           .WillCascadeOnDelete();

            modelBuilder.Entity<Author>().HasMany(c => c.Preferences)
                .WithMany(s => s.Authors)
                .Map(t => t.MapLeftKey("AuthorId")
                .MapRightKey("TagId")
                .ToTable("AuthorTag"));

            modelBuilder.Entity<Post>().HasMany(c => c.Tags)
              .WithMany(s => s.Posts)
              .Map(t => t.MapLeftKey("PostId")
              .MapRightKey("TagId")
              .ToTable("PostTag"));

            modelBuilder.Entity<Author>()
            .HasMany(c => c.Ratings)
          .WithRequired(o => o.Author)
          .WillCascadeOnDelete(false);

            modelBuilder.Entity<Post>()
            .HasMany(c => c.Ratings)
            .WithRequired(o => o.Post)
               .WillCascadeOnDelete();

            base.OnModelCreating(modelBuilder);
        }
    }
}
