﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.DAL.Entities;

namespace WebPhotoAlbum.DAL.Identity
{
    public class ApplicationRoleManager : RoleManager<CustomRole, int>, IDisposable
    {
        public ApplicationRoleManager(RoleStore<CustomRole, int, CustomUserRole> store)
            : base(store)
        { }

        public static ApplicationRoleManager Create(
            IdentityFactoryOptions<ApplicationRoleManager> options,
            IOwinContext context)
        {
            return new ApplicationRoleManager(new
                RoleStore<CustomRole, int, CustomUserRole>(context.Get<EFDbContext>()));
        }
    }
}
