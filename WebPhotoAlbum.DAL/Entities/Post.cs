﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebPhotoAlbum.DAL.Entities
{
    public class Post
    {
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }

      
        [HiddenInput(DisplayValue = false)]
        public byte[] ImageData { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ImageMimeType { get; set; }
        
  
        [HiddenInput(DisplayValue = false)]
        public bool IsDeleted { get; set; }
       
        public int AuthorId { get; set; }

        public virtual Author Author { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }

        public virtual ICollection<Rating> Ratings { get; set; }

        public Post()
        {
            Tags = new List<Tag>();
            Ratings = new List<Rating>();
        }
    }
}
