﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPhotoAlbum.DAL.Entities
{
   public class Rating
    {
        [Key]
        public int Id { get; set; }

        public bool IsLiked { get; set; }

        public bool IsDisliked { get; set; }

    
        public int AuthorId { get; set; }

        public virtual Author Author { get; set; }

   
        public int PostId { get; set; }

   
        public virtual Post Post { get; set; }

        public Rating()
        {
           
        }
    }
}
