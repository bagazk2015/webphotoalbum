﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebPhotoAlbum.DAL.Entities
{
   public class Tag
    {
        public int Id { get; set; }

        public string Title { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsDeleted { get; set; }

        public virtual ICollection<Author> Authors { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

       
        public Tag()
        {
            Posts = new List<Post>();
            Authors = new List<Author>();
       

        }

    }
}
