﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebPhotoAlbum.DAL.Entities
{
    public class Author
    {

        [Key]
        public int Id { get; set; }

        [ForeignKey("Id")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Surname")]
        public string Surname { get; set; }
        
        [HiddenInput(DisplayValue = false)]
        public bool IsDeleted { get; set; }



        public virtual ICollection<Post> Posts { get; set; }
        
        public virtual ICollection<Tag> Preferences { get; set; }

        public virtual ICollection<Rating> Ratings { get; set; }

        public Author()
        {

            Posts = new List<Post>();
            Preferences = new List<Tag>();
            Ratings = new List<Rating>();
        
        }

    }
}
