﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Identity;
using WebPhotoAlbum.DAL.Interfaces;

namespace WebPhotoAlbum.DAL.Repositories
{
    public class IdentityUnitOfWork :IIdentityUnitOfWork
    {
        private EFDbContext db;

        private ApplicationUserManager userManager;
        private ApplicationRoleManager roleManager;
   

        public IdentityUnitOfWork()
        {
            db = new EFDbContext();
            userManager = new ApplicationUserManager(new CustomUserStore(db));
            roleManager = new ApplicationRoleManager(new
                RoleStore<CustomRole, int, CustomUserRole>(db));
          
        }

        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }

     

        public ApplicationRoleManager RoleManager
        {
            get { return roleManager; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

  
    }
}
