﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Interfaces;

namespace WebPhotoAlbum.DAL.Repositories
{
   public class UnitOfWork:IUnitOfWork
    { 
        private static EFDbContext db = new EFDbContext();
        IGenericRepository<Author> repositoryAuthor;
        IGenericRepository<Admin> repositoryAdmin;
        IGenericRepository<Post> repositoryPost;
        IGenericRepository<Rating> repositoryRating;
        IGenericRepository<Tag> repositoryTag;

        public UnitOfWork()
        {

        }
        public UnitOfWork(EFDbContext eFDbContext)
        {
            db = eFDbContext;
        }

        public IGenericRepository<Author> Authors
        {
            get
            {
                if (repositoryAuthor == null)
                    repositoryAuthor = new GenericRepository<Author>(db);
                return repositoryAuthor;
            }
        }
        public IGenericRepository<Admin> Admins
        {
            get
            {
                if (repositoryAdmin == null)
                    repositoryAdmin = new GenericRepository<Admin>(db);
                return repositoryAdmin;
            }
        }
        public IGenericRepository<Post> Posts
        {
            get
            {
                if (repositoryPost == null)
                    repositoryPost = new GenericRepository<Post>(db);
                return repositoryPost;
            }
        }
        public IGenericRepository<Rating> Ratings
        {
            get
            {
                if (repositoryRating == null)
                    repositoryRating = new GenericRepository<Rating>(db);
                return repositoryRating;
            }
        }
        public IGenericRepository<Tag> Tags
        {
            get
            {
                if (repositoryTag == null)
                    repositoryTag = new GenericRepository<Tag>(db);
                return repositoryTag;
            }
        }
      
        public void Save()
        {
            db.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}