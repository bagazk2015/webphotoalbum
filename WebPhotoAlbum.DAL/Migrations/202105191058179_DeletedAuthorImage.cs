﻿namespace WebPhotoAlbum.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DeletedAuthorImage : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Authors", "ImageData");
            DropColumn("dbo.Authors", "ImageMimeType");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Authors", "ImageMimeType", c => c.String());
            AddColumn("dbo.Authors", "ImageData", c => c.Binary());
        }
    }
}
