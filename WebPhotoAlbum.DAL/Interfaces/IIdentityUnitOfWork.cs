﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.DAL.Identity;

namespace WebPhotoAlbum.DAL.Interfaces
{
   public interface IIdentityUnitOfWork
    {
        ApplicationUserManager UserManager { get; }
        ApplicationRoleManager RoleManager { get; }
       
        Task SaveAsync();
    
    }
}
