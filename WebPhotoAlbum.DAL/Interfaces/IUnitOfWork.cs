﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Identity;

namespace WebPhotoAlbum.DAL.Interfaces
{
   public interface IUnitOfWork
    {  
       
       
        IGenericRepository<Admin> Admins { get; }
        IGenericRepository<Author> Authors { get; }
        IGenericRepository<Post> Posts { get; }
        IGenericRepository<Tag> Tags { get; }
        IGenericRepository<Rating> Ratings { get; }
        void Save();
        void Dispose(bool disposing);
        void Dispose();

    }
}
