﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Infrastructure;

namespace WebPhotoAlbum.BLL.Interfaces.Services
{
   public interface IAuthorService:IUserIdentityService
    {
        Task<OperationDetails> Create(AuthorDTO authorDto);
        Task SetInitialData(AuthorDTO authorDto, List<string> roles);

        /// <summary>
        ///Returns author by id. 
        ///</summary>
        ///<param name="id">Id of author.</param>
        AuthorDTO GetAuthor(int id);
        /// <summary>
        ///Returns all authors. 
        ///</summary>
        List<AuthorDTO> GetAuthors();
        /// <summary>
        ///Deletes author by id. 
        ///</summary>
        ///<param name="id">Id of author.</param>
        void DeleteAuthor(int id);

        /// <summary>
        ///Updates author. 
        ///</summary>
        ///<param name="authorDTO">Edited author profile.</param>
        OperationDetails UpdateAuthor(UpdateAuthorDTO authorDTO);

    }
}
