﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPhotoAlbum.BLL.Interfaces.Services
{
   public interface IUserServiceCreator
    {
        IAdminService CreateAdminService();

        IAuthorService CreateAuthorService();

    }
}
