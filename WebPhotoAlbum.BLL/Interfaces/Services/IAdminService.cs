﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Infrastructure;

namespace WebPhotoAlbum.BLL.Interfaces.Services
{
   public interface IAdminService:IUserIdentityService
    {
        Task<OperationDetails> Create(AdminDTO adminDto);
        Task SetInitialData(AdminDTO adminDto, List<string> roles);

        List<AdminDTO> GetAdmins();
  
    }
}
