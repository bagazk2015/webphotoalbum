﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;

namespace WebPhotoAlbum.BLL.Interfaces.Services
{
   public interface IApplicationUserService
    {

        /// <summary>
        ///Finds user and returns role of this user. 
        ///</summary>
        ///<param name="email">Email of user.</param>
        string GetUserRoleByEmail(string email);

    }
}
