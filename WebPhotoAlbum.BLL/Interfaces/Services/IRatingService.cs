﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPhotoAlbum.BLL.Interfaces.Services
{
   public interface IRatingService
    {  
        /// <summary>
        ///The method save that user set like. 
        ///</summary>
        ///<param name="postId">Id of author.</param>
        ///<param name="userId">Id of post.</param>
        void SetLike(int postId, int userId);
        /// <summary>
        ///The method save that user set dislike. 
        ///</summary>
        ///<param name="postId">Id of author.</param>
        ///<param name="userId">Id of post.</param>
        void SetDislike(int postId, int userId);
        /// <summary>
        ///The method determines whether the current user has disliked the post. 
        ///</summary>
        ///<param name="authorId">Id of author.</param>
        ///<param name="postId">Id of post.</param>
        bool IsDislikedByAuthor(int authorId, int postId);
        /// <summary>
        ///The method determines whether the current user has liked the post. 
        ///</summary>
        ///<param name="authorId">Id of author.</param>
        ///<param name="postId">Id of post.</param>
        bool IsLikedByAuthor(int authorId, int postId);


        /// <summary>
        ///The method which count raiting.
        ///</summary>
        ///<param name="postId">Id of post.</param>
        string CountRating(int postId);

    }
}
