﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;

namespace WebPhotoAlbum.BLL.Interfaces.Services
{
    public interface ITagService
    {     /// <summary>
          ///Create new tag. 
          ///</summary>
          ///<param name="tagDTO">New tag dto.</param>
          ///<returns>Created tag</returns>
        TagDTO Create(TagDTO tagDTO);
    
        /// <summary>
        ///Create new tag and retutns it. 
        ///</summary>
        ///<param name="tagDTO">New tag dto.</param>
        void Create(ICollection<TagDTO> tagsDTO);

        /// <summary>
        ///Search tags by title. 
        ///</summary>
        ///<param name="searchText">The text that must contain tag title.</param>
        ///<returns>List of searched tags</returns>
        List<TagDTO> SearchByTitle(string searchText);

    }
}
