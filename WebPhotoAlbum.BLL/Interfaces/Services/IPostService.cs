﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Infrastructure;

namespace WebPhotoAlbum.BLL.Interfaces.Services
{
   public interface IPostService
    {      
        /// <summary>
        ///The method create new post. 
        ///</summary>
        ///<param name="postDTO">New post.</param>
        PostDTO Create(PostDTO postDTO);
        /// <summary>
        ///The method returns the specified number of posts. 
        ///</summary>
        ///<param name="count">Required number of posts.</param>
        List<PostDTO> GetPostsNumber(int count);
        /// <summary>
        ///Delete post by id. 
        ///</summary>
        ///<param name="id">Id of post.</param>
        void Delete(int Id);
        /// <summary>
        ///Find post by id. 
        ///</summary>
        ///<param name="id">Requered post id.</param>
        PostDTO FindById(int id);
        /// <summary>
        ///Find post by id. 
        ///</summary>
        ///<param name="id">Required post id.</param>
        Task<PostDTO> FindByIdAsync(int id);

        /// <summary>
        ///Find post by id with includes. 
        ///</summary>
        ///<param name="id">Required post id.</param>
        PostDTO FindByIdWithIncludes(int id);
        /// <summary>
        ///Returns the specified number of posts, skipping the specified number and filter by some text. 
        ///</summary>
        ///<param name="skip">Number of posts to skip.</param>
        ///<param name="take">Number of posts to take.</param>
        ///<param name="searchText">Filter text.</param>
        List<PostDTO> GetPostsWithSkip(int skip, int take, string searchText);

        /// <summary>
        ///Returns author`s posts. 
        ///</summary>
        ///<param name="authorId">Id of the author whose posts need to be returned.</param>
        List<PostDTO> GetPostsByAuthor(int authorId);

        
    }
}
