﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.BLL.Services;
using WebPhotoAlbum.DAL.Interfaces;
using WebPhotoAlbum.DAL.Repositories;

namespace WebPhotoAlbum.BLL.Util
{
  public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Unbind<ModelValidatorProvider>();
            Bind<IUnitOfWork>().To<UnitOfWork>();
            Bind<IIdentityUnitOfWork>().To<IdentityUnitOfWork>();
            Bind<IPostService>().To<PostService>();
            Bind<ITagService>().To<TagService>();
            Bind<IRatingService>().To<RatingService>();
            Bind<IApplicationUserService>().To<ApplicationUserService>();
            Bind<IAdminService>().To<AdminService>().WithConstructorArgument("identityUOW",new IdentityUnitOfWork()).WithConstructorArgument("uow", new UnitOfWork());
            Bind<IAuthorService>().To<AuthorService>().WithConstructorArgument("identityUOW", new IdentityUnitOfWork()).WithConstructorArgument("uow", new UnitOfWork()); ;
        }
    }
}
