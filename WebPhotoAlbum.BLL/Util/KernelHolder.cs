﻿using Ninject;
using Ninject.Modules;
using WebPhotoAlbum.BLL.Interfaces.Services;

namespace WebPhotoAlbum.BLL.Util
{
    public static class KernelHolder
    {
        static StandardKernel kernel;

        public static StandardKernel Kernel
        {
            get
            {
                if (kernel == null)
                {

                    NinjectModule registrations = new NinjectRegistrations();
                    kernel = new StandardKernel(registrations);

                }
                return kernel;
            }
        }

        public static IAdminService CreateAdminService()
        {
            return KernelHolder.Kernel.Get<IAdminService>();
        }
        public static IAuthorService CreateAuthorService()
        {
            return KernelHolder.Kernel.Get<IAuthorService>();
        }
    }
}
