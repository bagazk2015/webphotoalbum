﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPhotoAlbum.BLL.DTO
{
   public class PostDTO
    {
      
        public int Id { get; set; }

        public string Description { get; set; }

       
        public byte[] ImageData { get; set; }

 
        public string ImageMimeType { get; set; }

        public bool IsDeleted { get; set; }

     
        public int AuthorId { get; set; }


        public AuthorDTO Author { get; set; }

        public ICollection<TagDTO> Tags { get; set; }

        public ICollection<RatingDTO> Ratings { get; set; }

        public PostDTO()
        {
            Tags = new List<TagDTO>();
            Ratings = new List<RatingDTO>();
        }
    }
}
