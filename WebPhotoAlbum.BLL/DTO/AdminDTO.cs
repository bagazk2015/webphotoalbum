﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPhotoAlbum.BLL.DTO
{
   public class AdminDTO: ApplicationUserDTO
    {
  
        public string Name { get; set; }
     
        public string Surname { get; set; }
  
        public string Patronymic { get; set; }

        public bool IsDeleted { get; set; }

 

    }
}
