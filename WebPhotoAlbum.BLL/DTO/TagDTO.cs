﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPhotoAlbum.BLL.DTO
{
   public class TagDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

    }
}
