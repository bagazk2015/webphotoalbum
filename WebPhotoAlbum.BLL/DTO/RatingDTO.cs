﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPhotoAlbum.BLL.DTO
{
   public class RatingDTO
    {
        public int Id { get; set; }

        public bool IsLiked { get; set; }

        public bool IsDisliked { get; set; }


        public int AuthorId { get; set; }

        public int PostId { get; set; }


    }
}
