﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace WebPhotoAlbum.BLL.DTO
{
   public class AuthorDTO:ApplicationUserDTO
    {

        public string Name { get; set; }

        public string Surname { get; set; }

        public bool IsDeleted { get; set; }


        public ICollection<PostDTO> Posts { get; set; }

        public ICollection<TagDTO> Preferences { get; set; }

        public ICollection<RatingDTO> Ratings { get; set; }

        public AuthorDTO()
        {

            Posts = new List<PostDTO>();
            Preferences = new List<TagDTO>();
            Ratings = new List<RatingDTO>();

        }
    }
}
