﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.DAL.Interfaces;
using WebPhotoAlbum.DAL.Repositories;

namespace WebPhotoAlbum.BLL.Services
{
  public class UserServiceCreator:IUserServiceCreator
    {
        private IIdentityUnitOfWork IdentityUnitOfWork { get; }
        private IUnitOfWork UnitOfWork { get; }
        public UserServiceCreator(IIdentityUnitOfWork identityUnitOfWork,IUnitOfWork unitOfWork)
        {
            IdentityUnitOfWork = identityUnitOfWork;
            UnitOfWork = unitOfWork;
        }
        public IAdminService CreateAdminService()
        {
            return new AdminService(IdentityUnitOfWork,UnitOfWork);
        }

        public IAuthorService CreateAuthorService()
        {
            return new AuthorService(IdentityUnitOfWork, UnitOfWork);
        }
      
    }
}
