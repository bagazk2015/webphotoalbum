﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Interfaces;

namespace WebPhotoAlbum.BLL.Services
{
    public class TagService : ITagService
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly IMapper _mapper;

        public TagService(IUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            _mapper = new MapperConfiguration(cgf =>
            {


                cgf.CreateMap<Tag, TagDTO>().ReverseMap();


            }).CreateMapper();
        }

        public TagDTO Create(TagDTO tagDTO)
        {
            Tag tagSearch = unitOfWork.Tags.Get(x => x.Title == tagDTO.Title).FirstOrDefault();
            if (tagSearch == null)
            {
                Tag tag = _mapper.Map<TagDTO, Tag>(tagDTO);
                unitOfWork.Tags.Create(tag);
                return _mapper.Map<Tag, TagDTO>(tag);
            }
            return _mapper.Map<Tag, TagDTO>(tagSearch);

        }
       

        public void Create(ICollection<TagDTO> tagsDTO)
        {
            foreach(TagDTO tagDTO in tagsDTO)
            {
                Create(tagDTO);
            }
          
        }

        public List<TagDTO> SearchByTitle(string searchText)
        {
           List<Tag> tags = unitOfWork.Tags.Get(x => x.Title.ToLower().Contains(searchText.ToLower())).ToList();
            return _mapper.Map<List<Tag>, List<TagDTO>>(tags);
        }
    }
}
