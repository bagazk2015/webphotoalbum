﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Infrastructure;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Interfaces;

namespace WebPhotoAlbum.BLL.Services
{
    public class PostService : IPostService
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly IMapper _mapper;

        private readonly IMapper _mapperCreatePost;

        private readonly ITagService tagService;

        public PostService(IUnitOfWork _unitOfWork,ITagService _tagService)
        {
            unitOfWork = _unitOfWork;

            tagService = _tagService;

            _mapper = new MapperConfiguration(cgf =>
            {
               
                cgf.CreateMap<Post, PostDTO>()
                .ForMember(dest => dest.Tags,
                      (options) => options.Ignore())
                .ForMember(dest => dest.Ratings,
                   (options) => options.Ignore())
                 .ForMember(dest => dest.Author,
                      (options) => options.Ignore()).ReverseMap();
         

            }).CreateMapper();
            _mapperCreatePost = new MapperConfiguration(cgf =>
            {
                
  
           
                cgf.CreateMap<Author, AuthorDTO>().ForMember(dest => dest.Posts,
                   (options) => options.Ignore()).ForMember(dest => dest.Preferences,
                   (options) => options.Ignore()).ReverseMap();

                cgf.CreateMap<Tag, TagDTO>().ReverseMap();
                cgf.CreateMap<Rating, RatingDTO>().ReverseMap();
                cgf.CreateMap<Post, PostDTO>().ForMember(dest => dest.Ratings,
                      (options) => options.Ignore()).ReverseMap();
               

            }).CreateMapper();

         
        }
        /// <summary>
        ///The method create new post. 
        ///</summary>
        ///<param name="postDTO">New post.</param>

        public PostDTO Create(PostDTO postDTO)
        {
            Post post = _mapperCreatePost.Map<PostDTO, Post>(postDTO);
            post.Tags = new List<Tag>();
            unitOfWork.Posts.Create(post);

            tagService.Create(postDTO.Tags);
       
            foreach (TagDTO tagDTO in  postDTO.Tags)
            {
                Tag tag = unitOfWork.Tags.Get(x => x.Title == tagDTO.Title).FirstOrDefault();
                post.Tags.Add(unitOfWork.Tags.FindById(tag.Id));
                
            }
                unitOfWork.Posts.Update(post);
                 unitOfWork.Save();

            return _mapperCreatePost.Map<Post, PostDTO>(post);
        }
        /// <summary>
        ///The method returns the specified number of posts. 
        ///</summary>
        ///<param name="count">Required number of posts.</param>
        public List<PostDTO> GetPostsNumber(int count)
        {
            List<Post> posts = unitOfWork.Posts.Get(x=>x.IsDeleted!=true).Take(count).ToList();
          return _mapper.Map<List<Post>,List<PostDTO>>(posts);
        }
        /// <summary>
        ///Delete post by id. 
        ///</summary>
        ///<param name="id">Id of post.</param>
        public void Delete(int id)
        {
       
           Post post=unitOfWork.Posts.FindById(id);
            if(post!=null)
            {
                post.IsDeleted = true;
                unitOfWork.Posts.Update(post);
                unitOfWork.Save();
            }
         
        }
        /// <summary>
        ///Returns the specified number of posts, skipping the specified number and filter by some text. 
        ///</summary>
        ///<param name="skip">Number of posts to skip.</param>
        ///<param name="take">Number of posts to take.</param>
        ///<param name="searchText">Filter text.</param>
        public List<PostDTO> GetPostsWithSkip(int skip,int take,string searchText)
        {
          List<Post> posts = unitOfWork.Posts.GetRangeWithInclude(skip,take,x =>((x.Description == null ? "" : x.Description).Contains(searchText) || x.Tags.Select(desc => desc.Title)
                                                                                                                                              .Where(o => o.Contains(searchText))
                                                                                                                                              .Count() > 0)&&x.IsDeleted!=true,p=>p.Tags)
                                                                                                                                              .ToList();
        
                                                                       
          
            return _mapper.Map<List<Post>, List<PostDTO>>(posts); 
        }

        /// <summary>
        ///Find post by id. 
        ///</summary>
        ///<param name="id">Requered post id.</param>
     
        public PostDTO FindById(int id)
        {
            Post post = unitOfWork.Posts.FindById(id);
         return _mapper.Map<Post,PostDTO>(post);
        }
        /// <summary>
        ///Find post by id. 
        ///</summary>
        ///<param name="id">Required post id.</param>
        public async Task<PostDTO> FindByIdAsync(int id)
        {
            Post post =await unitOfWork.Posts.FindByIdAsync(id);
            return _mapper.Map<Post, PostDTO>(post);
        }
        /// <summary>
        ///Returns author`s posts. 
        ///</summary>
        ///<param name="authorId">Id of the author whose posts need to be returned.</param>
        public List<PostDTO> GetPostsByAuthor(int authorId)
        {
            List<Post> posts = unitOfWork.Posts.Get(x => x.AuthorId == authorId&&x.IsDeleted!=true).ToList();

            return _mapperCreatePost.Map<List<Post>, List<PostDTO>>(posts);

        }
        /// <summary>
        ///Find post by id with includes. 
        ///</summary>
        ///<param name="id">Required post id.</param>
        public PostDTO FindByIdWithIncludes(int id)
        {
            Post post = unitOfWork.Posts.GetWithInclude(x=>x.Id==id,x=>x.Tags,x=>x.Ratings).FirstOrDefault();
        
            return _mapperCreatePost.Map<Post, PostDTO>(post);
        }
        private void ResizeImage(ref Image image)
        {
            int MaxImageSize = 1000;
            if (image.Height <= MaxImageSize && image.Width <= MaxImageSize)
                return;

            var ratio = image.Width >= image.Height
                ? (float)MaxImageSize / image.Width
                : (float)MaxImageSize / image.Height;
            var nWidth = ratio * image.Width;
            var nHeight = ratio * image.Height;

            var resultImage = new Bitmap((int)(nWidth), (int)(nHeight), PixelFormat.Format16bppRgb565);
            using (var gx = Graphics.FromImage(resultImage))
            {
                gx.Clear(Color.White);
                gx.DrawImage(image,
                    new RectangleF(0, 0, nWidth, nHeight),
                    new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
            }

            image.Dispose();
            image = resultImage;
        }

    }
}

