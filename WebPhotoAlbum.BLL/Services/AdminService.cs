﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.Adstract;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Infrastructure;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Interfaces;

namespace WebPhotoAlbum.BLL.Services
{
    public class AdminService :UserIdentityService,IAdminService
    {
        IUnitOfWork unitOfWork { get; set; }



        private readonly IMapper _mapper;

        public AdminService(IIdentityUnitOfWork identityUOW, IUnitOfWork uow):base(identityUOW)
        {
            unitOfWork = uow;
         
            _mapper = new MapperConfiguration(cgf =>
            {
                cgf.CreateMap<Author, AuthorDTO>();
                cgf.CreateMap<AuthorDTO, Author>();
                cgf.CreateMap<AdminDTO, Admin>().ReverseMap();
            }).CreateMapper();
        }
        /// <summary>
        ///Create new admin. 
        ///</summary>
        ///<param name="adminDto">Admin to initial.</param>
        public async Task<OperationDetails> Create(AdminDTO adminDto)
        {
            ApplicationUser user = await identityUow.UserManager.FindByEmailAsync(adminDto.Email);
            if (user == null)
            {
                user = new ApplicationUser { Email = adminDto.Email, UserName = adminDto.Email };
                var result = await identityUow.UserManager.CreateAsync(user, adminDto.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                // добавляем роль
                await identityUow.UserManager.AddToRoleAsync(user.Id, adminDto.Role);
                // создаем профиль клиента
                Admin admin = new Admin{ Id = user.Id, Name = adminDto.Name,Surname= adminDto.Surname,Patronymic= adminDto.Patronymic,IsDeleted=false };
                unitOfWork.Admins.Create(admin);
                await identityUow.SaveAsync();
                return new OperationDetails(true, "Регистрация успешно пройдена", "");
            }
            else
            {
                return new OperationDetails(false, "Пользователь с таким логином уже существует", "Email");
            }
        }
        /// <summary>
        ///Set initial data. 
        ///</summary>
        ///<param name="adminDto">Admin to initial.</param>

        public async Task SetInitialData(AdminDTO adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await identityUow.RoleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    role = new CustomRole { Name = roleName };
                    await identityUow.RoleManager.CreateAsync(role);
                }
            }
            await Create(adminDto);
        }

        public List<AdminDTO> GetAdmins()
        {
            List<Admin> admins = unitOfWork.Admins.Get().ToList();

            List<AdminDTO> adminDtos = _mapper.Map<List<Admin>, List<AdminDTO>>(admins);

            foreach (AdminDTO adminDTO in adminDtos)
            {
              adminDTO.Email = identityUow.UserManager.FindByIdAsync(adminDTO.Id).Result.Email;
               adminDTO.Role = identityUow.UserManager.GetRoles(adminDTO.Id).FirstOrDefault();
            }

            return adminDtos;

        }

    }
}
