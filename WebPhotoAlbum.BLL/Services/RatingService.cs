﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Interfaces;

namespace WebPhotoAlbum.BLL.Services
{
    public class RatingService : IRatingService
    {
        private readonly IUnitOfWork unitOfWork;

        private readonly IMapper _mapper;
        public RatingService(IUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

            _mapper = new MapperConfiguration(cgf =>
            {


                cgf.CreateMap<Post, PostDTO>().ReverseMap();


            }).CreateMapper();
        }
        /// <summary>
        ///The method counts raiting.
        ///</summary>
        ///<param name="postId">Id of post.</param>
        public string CountRating(int postId)
        {
            int ratingLikesCount = unitOfWork.Ratings.Get(x => x.IsLiked == true && x.PostId==postId).Count();
            int ratingDislikesCount = unitOfWork.Ratings.Get(x => x.IsDisliked == true && x.PostId == postId).Count();
          
                return (ratingLikesCount - ratingDislikesCount).ToString();
            
            

        }
        /// <summary>
        ///The method determines whether the current user has liked the post. 
        ///</summary>
        ///<param name="authorId">Id of author.</param>
        ///<param name="postId">Id of post.</param>
        public bool IsLikedByAuthor(int authorId,int postId)
        {
            Rating rating = unitOfWork.Ratings.Get(x => x.PostId == postId && x.AuthorId == authorId).FirstOrDefault();
            if(rating!=null)
            {
                return rating.IsLiked;
            }
            else
            {
                return false;
            }
  
        }
        /// <summary>
        ///The method determines whether the current user has disliked the post. 
        ///</summary>
        ///<param name="authorId">Id of author.</param>
        ///<param name="postId">Id of post.</param>
        public bool IsDislikedByAuthor(int authorId, int postId)
        {
            Rating rating = unitOfWork.Ratings.Get(x => x.PostId == postId && x.AuthorId == authorId).FirstOrDefault();
            if (rating != null)
            {
                return rating.IsDisliked;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        ///The method saves that user set dislike. 
        ///</summary>
        ///<param name="postId">Id of author.</param>
        ///<param name="userId">Id of post.</param>
        public void SetDislike(int postId, int userId)
        {
     
              Rating ratingFromPost = unitOfWork.Ratings.Get(x => x.AuthorId == userId && x.PostId==postId).FirstOrDefault();

            if(ratingFromPost == null)
            {
                Rating rating = new Rating();

                rating.IsDisliked = true;

                Post postById = unitOfWork.Posts.FindById(postId);

                rating.Post = postById;
                rating.Author = unitOfWork.Authors.FindById(userId);

                unitOfWork.Ratings.Create(rating);
            }
            else
            {
                Rating rating = unitOfWork.Ratings.FindById(ratingFromPost.Id);
                rating.IsLiked = false;
                if(rating.IsDisliked==false)
                {
                    rating.IsDisliked = true;
                }
                else
                {
                    rating.IsDisliked = false; ;
                }
                unitOfWork.Ratings.Update(rating);
                unitOfWork.Save();
            }

        }
        /// <summary>
        ///The method saves that user set like. 
        ///</summary>
        ///<param name="postId">Id of author.</param>
        ///<param name="userId">Id of post.</param>
        public void SetLike(int postId, int userId)
        {
            Rating ratingFromPost = unitOfWork.Ratings.Get(x => x.AuthorId == userId && x.PostId == postId).FirstOrDefault();

            if (ratingFromPost == null)
            {
                Rating rating = new Rating();

               rating.IsLiked = true;
                Post postById = unitOfWork.Posts.FindById(postId);
                rating.PostId = postId;
                rating.Post = postById;
                rating.AuthorId = userId;
                rating.Author = unitOfWork.Authors.FindById(userId);

                unitOfWork.Ratings.Create(rating);
                unitOfWork.Save();
            }
            else
            {
                Rating rating = unitOfWork.Ratings.FindById(ratingFromPost.Id);
                rating.IsDisliked = false;
                if (rating.IsLiked == false)
                {
                    rating.IsLiked = true;
                }
                else
                {
                    rating.IsLiked = false; 
                }
                unitOfWork.Ratings.Update(rating);
                unitOfWork.Save();

            }
        }
    }
}
