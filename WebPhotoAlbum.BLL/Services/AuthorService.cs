﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.Adstract;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Infrastructure;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Interfaces;

namespace WebPhotoAlbum.BLL.Services
{
    public class AuthorService : UserIdentityService, IAuthorService
    {
        IUnitOfWork unitOfWork { get; set; }
        private readonly IMapper _mapper;

        public AuthorService(IIdentityUnitOfWork identityUOW, IUnitOfWork uow) : base(identityUOW)
        {
            unitOfWork = uow;

            _mapper = new MapperConfiguration(cgf =>
            {
                cgf.CreateMap<Tag, TagDTO>().ReverseMap();
                cgf.CreateMap<Post, PostDTO>().ReverseMap();
                cgf.CreateMap<Rating, RatingDTO>().ReverseMap();
                cgf.CreateMap<ApplicationUser, AuthorDTO>().ReverseMap();
                cgf.CreateMap<Author, AuthorDTO>().ReverseMap();

                cgf.CreateMap<Author, AuthorDTO>().ForMember(dest => dest.Preferences,
                     opt => opt.MapFrom(
                         src => (_mapper.Map<IEnumerable<Tag>, IEnumerable<TagDTO>>(src.Preferences)))).ReverseMap();

                cgf.CreateMap<Author, AuthorDTO>().ForMember(dest => dest.Ratings,
                opt => opt.MapFrom(
                    src => (_mapper.Map<IEnumerable<Rating>, IEnumerable<RatingDTO>>(src.Ratings)))).ReverseMap();

            }).CreateMapper();
        }

        public async Task<OperationDetails> Create(AuthorDTO userDto)
        {
            ApplicationUser user = await identityUow.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                user = new ApplicationUser { Email = userDto.Email, UserName = userDto.Email };
                var result = await identityUow.UserManager.CreateAsync(user, userDto.Password);

                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");

                await identityUow.UserManager.AddToRoleAsync(user.Id, userDto.Role);
                userDto.Id = user.Id;
                unitOfWork.Authors.Create(_mapper.Map<AuthorDTO, Author>(userDto));
                await identityUow.SaveAsync();
                return new OperationDetails(true, "Регистрация успешно пройдена", "");
            }
            else
            {
                return new OperationDetails(false, "Пользователь с таким логином уже существует", "Email");
            }
        }


        public async Task SetInitialData(AuthorDTO authorDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await identityUow.RoleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    role = new CustomRole { Name = roleName };
                    await identityUow.RoleManager.CreateAsync(role);
                }
            }
            await Create(authorDto);
        }

        public AuthorDTO GetAuthor(int id)
        {
            Author author = unitOfWork.Authors.FindById(id);
            ApplicationUser applicationUser = identityUow.UserManager.FindById(id);
            AuthorDTO authorDto = _mapper.Map<ApplicationUser, AuthorDTO>(applicationUser);
            _mapper.Map(author, authorDto);

            return authorDto;

        }
        public List<AuthorDTO> GetAuthors()
        {
            List<Author> authors = unitOfWork.Authors.Get().ToList();

            List<AuthorDTO> authorDtos = _mapper.Map<List<Author>, List<AuthorDTO>>(authors);

            foreach (AuthorDTO authorDTO in authorDtos)
            {
                authorDTO.Email = identityUow.UserManager.FindByIdAsync(authorDTO.Id).Result.Email;
                authorDTO.Role = identityUow.UserManager.GetRoles(authorDTO.Id).FirstOrDefault();
            }

            return authorDtos;

        }
        /// <summary>
        ///Delete author by id. 
        ///</summary>
        ///<param name="id">The author Id of which you want to delete.</param>
        public void DeleteAuthor(int id)
        {
            ApplicationUser applicationUser = identityUow.UserManager.FindById(id);
            if (applicationUser != null)
            { 
                Author author = unitOfWork.Authors.FindById(id);
                if (author != null)
                {
               
                        author.IsDeleted = true;
                        unitOfWork.Authors.Update(author);
                        unitOfWork.Save();
                    

                    //    unitOfWork.Authors.Remove(author);  
                    //    unitOfWork.Save();
                    //identityUow.UserManager.DeleteAsync(applicationUser);
                    //    identityUow.SaveAsync();
                }

            }

        }

        public OperationDetails UpdateAuthor(UpdateAuthorDTO authorDTO)
        {
            Author author = new Author();
            ApplicationUser applicationUser = new ApplicationUser();
            if (authorDTO!=null)
            {
            author = unitOfWork.Authors.FindById(authorDTO.Id);
            applicationUser= identityUow.UserManager.FindById(authorDTO.Id);
            }
            else
            {
                 throw new ArgumentNullException();
            
            }
            if (author!=null&&applicationUser!=null)
            {
                author.Name = authorDTO.Name;
                author.Surname = authorDTO.Surname;
                if(authorDTO.Password!=null)
                {
                  var result= identityUow.UserManager.ChangePassword(applicationUser.Id,authorDTO.OldPassword, authorDTO.Password);
                    if (result.Errors.Count() > 0)
                        return new OperationDetails(false, result.Errors.FirstOrDefault(), "");


                }
                
                unitOfWork.Authors.Update(author);
                return new OperationDetails(true, "", "");
            }
            else
            {
                return new OperationDetails(false,"User didn`t found","Id");
            }

        }
    }

}
