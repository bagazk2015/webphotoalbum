﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.DAL.Entities;
using WebPhotoAlbum.DAL.Interfaces;

namespace WebPhotoAlbum.BLL.Services
{
   public class ApplicationUserService : IApplicationUserService
    {
        private readonly IIdentityUnitOfWork unitOfWork;

   
        public ApplicationUserService(IIdentityUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;

        }
        /// <summary>
        ///Finds user and returns role of this user. 
        ///</summary>
        ///<param name="email">Email of user.</param>
        public string GetUserRoleByEmail(string email)
        {
         ApplicationUser applicationUser = unitOfWork.UserManager.FindByEmailAsync(email).Result;
            if(applicationUser==null)
            {
                return "User does not exists";
            }
            List<string> roles = unitOfWork.UserManager.GetRoles(applicationUser.Id).ToList();
            if(roles!=null)
            {
                  return roles[0];
            }
            else
            {
                return "Without role";
            }
            

            
        }
    }
}
