﻿function SetLike(Id) {
    $.ajax({
        type: "Post",
        url: "/Rating/SetLike",
        data: { postId: Id },
        success: function (data, textStatus, xhr) {
            if (xhr.getResponseHeader("X-Responded-JSON") != null
                && JSON.parse(xhr.getResponseHeader("X-Responded-JSON")).status == "401") {
                window.location.href = '/Account/Login/'
            }
            $("#rating").html(data);
        }
    });

}
function SetDislike(Id) {
    $.ajax({
        type: "Post",
        url: "/Rating/SetDislike",
        data: { postId: Id },
        success: function (data, textStatus, xhr) {
            if (xhr.getResponseHeader("X-Responded-JSON") != null
                && JSON.parse(xhr.getResponseHeader("X-Responded-JSON")).status == "401") {
                window.location.href = '/Account/Login/'
            }
            $("#rating").html(data);
        }
    });

}
