﻿function showImages() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("content").style.display = "block";
}

function ShowPostModalToAuthor(Id) {



    $.ajax({
        type: "GET",
        url: "/Post/GetPostDisplayToAuthor",
        data: { Id: Id },

        success: function (data) {
            $("#ShowPostModal").html(data);
            $("#ShowPostModal").modal('show');
        }
    });

}
function showDropdown() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

function DeletePostModal(Id) {

    $.ajax({
        type: "GET",
        url: "/Post/DeleteConfirm",
        data: { Id: Id },

        success: function (data) {
            $("#ConfirmDeletePostModal").html(data);
            $("#ConfirmDeletePostModal").modal('show');
        }
    });

}