﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPhotoAlbum.Models.Home
{
    public class DisplayPostVM
    {

      public int Id { get; set; }
   

        public byte[] ImageData { get; set; }


        public string ImageMimeType { get; set; }

    
    }
}