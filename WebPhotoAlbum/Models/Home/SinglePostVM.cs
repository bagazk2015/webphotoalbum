﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPhotoAlbum.Models.Rating;
using WebPhotoAlbum.Models.Tag;

namespace WebPhotoAlbum.Models.Home
{
    public class SinglePostVM
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public List<TagVM> Tags { get; set; }

        public byte[] ImageData { get; set; }

        public RatingVM RatingVM { get; set; }
        public string ImageMimeType { get; set; }
    }
}