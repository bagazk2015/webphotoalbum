﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPhotoAlbum.Models.Home;

namespace WebPhotoAlbum.Models.AuthorProfile
{
    public class AuthorProfileVM
    {
        public int Id { get; set; }

        public string UserName { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public List<DisplayPostVM> DisplayPostVMs { get; set; }

    }
}