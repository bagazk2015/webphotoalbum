﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebPhotoAlbum.Attributes;
namespace WebPhotoAlbum.Models.Post
{
    public class PostVM
    {
        public int Id { get; set; }
        public string Description { get; set; }

        [Required]
        public HttpPostedFileBase InputImage { get; set; }

    }
}