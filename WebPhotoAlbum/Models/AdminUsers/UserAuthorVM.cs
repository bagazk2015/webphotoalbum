﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPhotoAlbum.Models.AdminUsers
{
    public class UserAuthorVM
    {
        public int Id { get; set; }
        public string UserName { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public string Role { get; set; }

        public bool IsDeleted { get; set; }

    }
}