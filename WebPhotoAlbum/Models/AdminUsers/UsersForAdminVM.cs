﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPhotoAlbum.Models.AdminUsers
{
    public class UsersForAdminVM
    {
        public List<UserAuthorVM> Admins { get; set; }

        public List<UserAuthorVM> Authors { get; set; }

    }
}