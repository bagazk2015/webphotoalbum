﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPhotoAlbum.Models.Rating
{
    public class RatingVM
    {
        public int PostId { get; set; }
        public bool IsLiked { get; set; }

        public bool IsDisliked { get; set; }

        public string Likes { get; set; }
    }
}