﻿using System.Web;
using System.Web.Optimization;

namespace WebPhotoAlbum
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. на странице https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // готово к выпуску, используйте средство сборки по адресу https://modernizr.com, чтобы выбрать только необходимые тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/home").Include(
                   "~/Scripts/Scripts/Home.js"));
            bundles.Add(new ScriptBundle("~/bundles/post").Include(
           "~/Scripts/Scripts/Post.js"));
            bundles.Add(new ScriptBundle("~/bundles/rating").Include(
    "~/Scripts/Scripts/Rating.js"));
            bundles.Add(new ScriptBundle("~/bundles/authorProfile").Include(
"~/Scripts/Scripts/AuthorProfile.js"));
            bundles.Add(new ScriptBundle("~/bundles/homeScroll").Include(
"~/Scripts/Scripts/HomeScroll.js"));
        }
    }
}
