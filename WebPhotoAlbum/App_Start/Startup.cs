﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.BLL.Services;
using WebPhotoAlbum.BLL.Util;

namespace WebPhotoAlbum.App_Start
{
    public class Startup
    {


        public void Configuration(IAppBuilder app)
        {


            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Error/Forbidden"),
            });

        }


    }

}
