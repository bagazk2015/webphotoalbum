﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPhotoAlbum.Models.Home;

namespace WebPhotoAlbum.Helpers
{
    public static class MansotyGrid
    {

        public static MvcHtmlString CreateGrid(this HtmlHelper html, List<DisplayPostVM> items)
        {


            TagBuilder tagLayout = new TagBuilder("div");
            tagLayout.AddCssClass("grid-layout");
            tagLayout.MergeAttribute("id","imagesGrid");
         
            int counter = 0;
            int expected =10;
            bool span2 = true;
            foreach (DisplayPostVM item in items)
            {
                counter++;
                TagBuilder block = new TagBuilder("div");
                block.MergeAttribute("onclick","ShowPostModal("+item.Id.ToString()+")");
                if (counter >= expected)
                {
                
            
                        if (span2 == true)
                        {
                            block.AddCssClass("span-2");
                            span2 = false;
                            counter = 0;
                        }
               
                    

                }
              block.AddCssClass("grid-item");
                TagBuilder img = new TagBuilder("img");
                var urlHelper = new UrlHelper(html.ViewContext.RequestContext);
                var url =  urlHelper.Action("GetPostImageToGridItem", "Post", new { Id=item.Id});


                img.MergeAttribute("src", url);
                 img.AddCssClass("block-img");
             
                block.InnerHtml += img.ToString();

                tagLayout.InnerHtml += block.ToString();
            }
            return new MvcHtmlString(tagLayout.ToString());

        }
        public static MvcHtmlString CreateList(this HtmlHelper html, List<DisplayPostVM> items)
        {
            string htmlString="";
            int counter = 0;
            int expected = 10;
            bool span2 = true;
            foreach (DisplayPostVM item in items)
            {
                counter++;
                TagBuilder block = new TagBuilder("div");
                block.MergeAttribute("onclick", "ShowPostModal(" + item.Id.ToString() + ")");
                if (counter >= expected)
                {

                        block.AddCssClass("span-2");
                        span2 = false;
                        counter = 0;
                }
                block.AddCssClass("grid-item");
                TagBuilder blockLoader = new TagBuilder("div");
                blockLoader.AddCssClass("loader-wrapper");
                TagBuilder loaderImg = new TagBuilder("div");
                loaderImg.AddCssClass("loader-image");
                blockLoader.InnerHtml += loaderImg;
                blockLoader.MergeAttribute("id", "imageLoader");
                TagBuilder img = new TagBuilder("img");
                var urlHelper = new UrlHelper(html.ViewContext.RequestContext);
                var url = urlHelper.Action("GetPostImageToGridItem", "Post", new { Id = item.Id });

               
                img.MergeAttribute("src", url);
                img.MergeAttribute("onload", "ShowImage(this)");
          
                img.AddCssClass("block-img");
                img.AddCssClass("hideImg");
                block.InnerHtml += blockLoader.ToString();
                block.InnerHtml += img.ToString();

                htmlString += block.ToString();
            }
            return new MvcHtmlString(htmlString);

        }
        public static MvcHtmlString CreateGridForAuthor(this HtmlHelper html, List<DisplayPostVM> items)
        {


            TagBuilder tagLayout = new TagBuilder("div");
            tagLayout.AddCssClass("grid-layout");
            tagLayout.MergeAttribute("id", "imagesGrid");

            int counter = 0;
           
            foreach (DisplayPostVM item in items)
            {
                counter++;
                TagBuilder block = new TagBuilder("div");
                block.MergeAttribute("onclick", "ShowPostModalToAuthor(" + item.Id.ToString() + ")");
               
                block.AddCssClass("grid-item");
                TagBuilder img = new TagBuilder("img");
                var urlHelper = new UrlHelper(html.ViewContext.RequestContext);
                var url = urlHelper.Action("GetAuthorPostImageToGridItem", "Post", new { Id = item.Id });


                img.MergeAttribute("src", url);
                img.AddCssClass("block-img");

                block.InnerHtml += img.ToString();

                tagLayout.InnerHtml += block.ToString();
            }
            return new MvcHtmlString(tagLayout.ToString());

        }
    }
}
