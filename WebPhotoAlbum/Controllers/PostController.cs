﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.Helpers;
using WebPhotoAlbum.Models.Home;
using WebPhotoAlbum.Models.Post;
using WebPhotoAlbum.Models.Rating;
using WebPhotoAlbum.Models.Tag;

namespace WebPhotoAlbum.Controllers
{
    [Authorize(Roles = "Author")]
    public class PostController : Controller
    {
        // GET: Post
        private readonly IPostService postService;

        private readonly IAuthorService authorService;

        private readonly IRatingService ratingService;

        private readonly IMapper _mapper;
        public PostController(IPostService _postService, IAuthorService _authorService, IRatingService _ratingService)
        {
            postService = _postService;

            authorService = _authorService;

            ratingService = _ratingService;

            _mapper = new MapperConfiguration(cgf =>
            {
                cgf.CreateMap<PostDTO, PostVM>().ReverseMap();
                cgf.CreateMap<TagDTO, TagVM>().ReverseMap();
                cgf.CreateMap<PostDTO, SinglePostVM>().ForMember(dest => dest.Tags,
                      opt => opt.MapFrom(
                         src => (_mapper.Map<ICollection<TagDTO>, ICollection<TagVM>>(src.Tags)))).ReverseMap();
                cgf.CreateMap<PostDTO, DisplayPostVM>().ReverseMap();

            }).CreateMapper();
        }
       
        public ActionResult Index()
        {

            return View(new PostVM());
        }
        /// <summary>
        ///Returns partial view with new post modal. 
        ///</summary>

        ///<returns> Partial view with new post modal</returns>
        [HttpGet]
        public ActionResult CreatePost()
        {

            Session["Tags"] = new List<TagVM>();
            return PartialView("~/Views/Post/Partial/NewPostPartialView.cshtml", new PostVM());

        }
        /// <summary>
        ///Create new post. 
        ///</summary>
        ///<param name="postVM">New post.</param>
        ///<returns>Redirect to index author profile</returns>
        [HttpPost]
        public ActionResult CreatePost(PostVM postVM)
        {
            if (ModelState.IsValid)
            {

                PostDTO postDTO = new PostDTO();
                postDTO.Description = postVM.Description;
                Bitmap bmp;


                if (postVM.InputImage != null)
                {
                    postDTO.ImageMimeType = postVM.InputImage.ContentType;
                    postDTO.ImageData = new byte[postVM.InputImage.ContentLength];
                    postVM.InputImage.InputStream.Read(postDTO.ImageData, 0, postVM.InputImage.ContentLength);
                }


                postDTO.Tags = _mapper.Map<ICollection<TagVM>, ICollection<TagDTO>>(Session["Tags"] as ICollection<TagVM>);
                postDTO.AuthorId = User.Identity.GetUserId<int>();

                postService.Create(postDTO);
            }
            return RedirectToAction("Index", "AuthorProfile");

        }
        /// <summary>
        ///Gets next posts and returns partial view with next posts. 
        ///</summary>
        ///<param name="pageCount">Number of page.</param>
        ///<param name="searchText">Text to search.</param>
        ///<returns> Partial view with next posts</returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult GetNextPosts(int pageCount, string searchText)
        {
            int pageSize = Int32.Parse(ConfigurationSettings.AppSettings["PageSize"]);
            List<PostDTO> postDTOs = postService.GetPostsWithSkip(pageCount *pageSize, pageSize, searchText);
            List<DisplayPostVM> postVMs = new List<DisplayPostVM>();

            _mapper.Map<List<PostDTO>, List<DisplayPostVM>>(postDTOs, postVMs);

            List<DisplayPostVM> displayPostVMs = (Session["NextPosts"] as List<DisplayPostVM>);
            displayPostVMs.AddRange(postVMs);
            Session["NextPosts"] = displayPostVMs;
            return PartialView("~/Views/Home/Partial/ListOfPostsView.cshtml", postVMs);
        }



        [HttpPost]
        public ActionResult DenyCreating(int Id)
        {
            postService.Delete(Id);

            return RedirectToAction("Index", "AuthorProfile");
        }
        /// <summary>
        ///Returns necessary image to home. 
        ///</summary>
        ///<param name="id">Id of post.</param>
        ///<returns>Necessary image</returns>
        [AllowAnonymous]
        public ActionResult GetPostImageToGridItem(int Id)
        {
          
            List<DisplayPostVM> displayPostVMs = (Session["NextPosts"] as List<DisplayPostVM>);

            if (displayPostVMs == null)
            {
                return RedirectToAction("Index");
            }
            DisplayPostVM post = displayPostVMs.FirstOrDefault(x => x.Id == Id);

            if (post != null)
            {
                if (post.ImageData != null || post.ImageMimeType != null)
                {
                    displayPostVMs.Remove(post);
                    Session["NextPosts"] = displayPostVMs;
                    return File(post.ImageData, post.ImageMimeType);
                }
                else
                {
                    PostDTO postDTO = postService.FindById(Id);
                    if (postDTO == null)
                    {
                        var dir = Server.MapPath("~/Content/Images");
                        var path = Path.Combine(dir, "gallery.png");
                        return File(path, "image/jpeg");
                    }
                    return File(postDTO.ImageData, postDTO.ImageMimeType);
                }
            }
            else
            {

                var dir = Server.MapPath("~/Content/Images");
                var path = Path.Combine(dir, "gallery.png");
                return File(path, "image/jpeg");
            }


        }

        /// <summary>
        ///Returns necessary image to author profile. 
        ///</summary>
        ///<param name="id">Id of post.</param>
        ///<returns>Necessary image</returns>
        [AllowAnonymous]
        public ActionResult GetAuthorPostImageToGridItem(int Id)
        {

            List<DisplayPostVM> displayPostVMs = (Session["NextAuthorPosts"] as List<DisplayPostVM>);

            if (displayPostVMs == null)
            {
                return RedirectToAction("Index");
            }
            DisplayPostVM post = displayPostVMs.FirstOrDefault(x => x.Id == Id);

            if (post != null)
            {
                if (post.ImageData != null || post.ImageMimeType != null)
                {

                    displayPostVMs.Remove(post);
                    Session["NextAuthorPosts"] = displayPostVMs;
                    return File(post.ImageData, post.ImageMimeType);
                }
                else
                {
                    PostDTO postDTO = postService.FindById(Id);
                    if (postDTO == null)
                    {
                        var dir = Server.MapPath("~/Content/Images");
                        var path = Path.Combine(dir, "gallery.png");
                        return File(path, "image/jpeg");
                    }
                    return File(postDTO.ImageData, postDTO.ImageMimeType);
                }
            }
            else
            {

                var dir = Server.MapPath("~/Content/Images");
                var path = Path.Combine(dir, "gallery.png");
                return File(path, "image/jpeg");
            }


        }

        /// <summary>
        ///Returns choosed on image grid post image. 
        ///</summary>
        ///<param name="id">Id of post.</param>
        ///<returns>Necessary image</returns>
        [AllowAnonymous]
        public ActionResult GetChoosedPostImage(int Id)
        {


            SinglePostVM post = (Session["ChoosedPost"] as SinglePostVM);

            if (post != null && post.ImageData != null && post.ImageMimeType != null)
            {
                return File(post.ImageData, post.ImageMimeType);
            }
            else
            {
                var dir = Server.MapPath("~/Content/Images/icons");
                var path = Path.Combine(dir, "UserHaveNoPhoto.jpg");
                return File(path, "image/jpeg");
            }

        }

        /// <summary>
        ///Create post view model. 
        ///</summary>
        ///<param name="id">Id of post.</param>
        ///<returns>Post view model </returns>
        private SinglePostVM CreatePostVM(int Id)
        {
            PostDTO postDTO = postService.FindByIdWithIncludes(Id);
            SinglePostVM singlePostVM = _mapper.Map<PostDTO, SinglePostVM>(postDTO);
            RatingVM ratingVM = new RatingVM();
            ratingVM.PostId = Id;
            ratingVM.Likes = ratingService.CountRating(Id);
            ratingVM.IsDisliked = ratingService.IsDislikedByAuthor(User.Identity.GetUserId<int>(), Id);
            ratingVM.IsLiked = ratingService.IsLikedByAuthor(User.Identity.GetUserId<int>(), Id);
            singlePostVM.RatingVM = ratingVM;
            return singlePostVM;
        }
        /// <summary>
        ///Returns choosed post partial view to home. 
        ///</summary>
        ///<param name="id">Id of post.</param>
        ///<returns>Partial view with necessary post</returns>
        [AllowAnonymous]
        public ActionResult GetPostDisplayToHome(int Id)
        {
            SinglePostVM singlePostVM = CreatePostVM(Id);
            Session["ChoosedPost"] = singlePostVM;
            return PartialView("~/Views/Post/Partial/DisplayPostView.cshtml", singlePostVM);
        }
        /// <summary>
        ///Returns choosed post partial view for author. 
        ///</summary>
        ///<param name="id">Id of post.</param>
        ///<returns>Partial view with necessary post</returns>
        public ActionResult GetPostDisplayToAuthor(int Id)
        {
            SinglePostVM singlePostVM = CreatePostVM(Id);
            Session["ChoosedPost"] = singlePostVM;
            return PartialView("~/Views/AuthorProfile/Partial/DisplayAuthorPostView.cshtml", singlePostVM);
        }
        /// <summary>
        ///Confirm deletion choosed post. 
        ///</summary>
        ///<param name="id">Id of post.</param>
        ///<returns>Partial view with delete confirmation</returns>
        public ActionResult DeleteConfirm(int Id)
        {
            
            DeletePostConfirmVM deletePostConfirmVM = new DeletePostConfirmVM();
            deletePostConfirmVM.Id = Id;

            return PartialView("~/Views/AuthorProfile/Partial/DeleteConfirm.cshtml", deletePostConfirmVM);

        }
        /// <summary>
        ///Delete choosed post. 
        ///</summary>
        ///<param name="deletePostConfirmVM">View model with deletion confirmation.</param>
        ///<returns>Redirect to index author profile</returns>
        public ActionResult DeletePost(DeletePostConfirmVM deletePostConfirmVM)
        {
            postService.Delete(deletePostConfirmVM.Id);
            return RedirectToAction("Index", "AuthorProfile");
        }

    }
}