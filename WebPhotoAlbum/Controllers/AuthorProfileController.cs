﻿
using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Infrastructure;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.Models.AuthorProfile;
using WebPhotoAlbum.Models.Home;

namespace WebPhotoAlbum.Controllers
{
    [Authorize(Roles = "Author")]
    public class AuthorProfileController : Controller
    {
        private readonly IAuthorService authorService;
        private readonly IMapper _mapper;
        private readonly IPostService postService;
        public AuthorProfileController(IAuthorService _authorService, IPostService _postService)
        {

            authorService = _authorService;
            postService = _postService;
            _mapper = new MapperConfiguration(cgf =>
            {
                cgf.CreateMap<AuthorDTO, AuthorProfileVM>().ReverseMap();
                cgf.CreateMap<AuthorDTO, EditAuthorProfileVM>().ReverseMap();
                cgf.CreateMap<UpdateAuthorDTO, EditAuthorProfileVM>().ReverseMap();
                cgf.CreateMap<PostDTO, DisplayPostVM>().ReverseMap();


            }).CreateMapper();
        }

        /// <summary>
        ///Returns view with author profile. 
        ///</summary>
        ///<param name="id">Post id.</param>
        ///<returns>Author profile view</returns>
        public ActionResult Index()
        {
            AuthorDTO authorDTO = authorService.GetAuthor(User.Identity.GetUserId<int>());
            if (authorDTO == null)
            {
                return RedirectToAction("Login", "Account");
            }
            AuthorProfileVM authorProfileVM = _mapper.Map<AuthorDTO, AuthorProfileVM>(authorDTO);
            List<PostDTO> postDTOs = postService.GetPostsByAuthor(User.Identity.GetUserId<int>());
            List<DisplayPostVM> postVMs = new List<DisplayPostVM>();
            if (authorProfileVM.DisplayPostVMs == null)
            {
                authorProfileVM.DisplayPostVMs = new List<DisplayPostVM>();
            }

            foreach (PostDTO postDTO in postDTOs)
            {
                DisplayPostVM displayPostVM = new DisplayPostVM()
                {
                    Id = postDTO.Id,
                    ImageData = postDTO.ImageData,
                    ImageMimeType = postDTO.ImageMimeType,

                };
                postVMs.Add(displayPostVM);


            }
            postVMs.Reverse();
            Session["NextAuthorPosts"] = postVMs;


            authorProfileVM.DisplayPostVMs = postVMs;
            return View(authorProfileVM);
        }
        /// <summary>
        ///Returns view with author profile for editing. 
        ///</summary>

        [HttpGet]
        public ActionResult EditAuthorProfile()
        {
            AuthorDTO authorDTO = authorService.GetAuthor(User.Identity.GetUserId<int>());
            if (authorDTO == null)
            {
                return RedirectToAction("Login", "Account");
            }
            EditAuthorProfileVM authorProfileVM = _mapper.Map<AuthorDTO, EditAuthorProfileVM>(authorDTO);

            return View(authorProfileVM);

        }
        /// <summary>
        ///Returns view with author profile for editing. 
        ///</summary>
        ///<param name="authorProfileVM">Edited profile data.</param>
        ///<returns>If succeed returns to author profile else shows errors on edit view</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAuthorProfile(EditAuthorProfileVM authorProfileVM)
        {
            if (ModelState.IsValid)
            {

                OperationDetails operationDetails = authorService.UpdateAuthor(_mapper.Map<EditAuthorProfileVM, UpdateAuthorDTO>(authorProfileVM));
                if (operationDetails.Succedeed)
                    return RedirectToAction("Index", "AuthorProfile");
                else
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
            }
            return View(authorProfileVM);

        }





    }
}
