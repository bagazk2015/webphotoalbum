﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.Models.Rating;

namespace WebPhotoAlbum.Controllers
{
    [Authorize(Roles = "Author")]
    public class RatingController : Controller
    {
        private IRatingService ratingService;
        public RatingController(IRatingService _ratingService)
        {
            ratingService = _ratingService;


        }
        /// <summary>
        ///Return rating of post. 
        ///</summary>
        ///<param name="postId">Post id.</param>
        ///<returns>Partial view with ratings</returns>
        public ActionResult GetRating(int postId)
        {
            RatingVM ratingVM = new RatingVM();
            ratingVM.PostId = postId;
            ratingVM.Likes=ratingService.CountRating(postId);
            ratingVM.IsDisliked=ratingService.IsDislikedByAuthor(User.Identity.GetUserId<int>(), postId);
            ratingVM.IsLiked = ratingService.IsLikedByAuthor(User.Identity.GetUserId<int>(), postId);
            return PartialView("~/Views/Post/Partial/DisplayRatingView.cshtml", ratingVM);
        }
        [Authorize(Roles = "Author")]
        /// <summary>
        ///Set like to post. 
        ///</summary>
        ///<param name="postId">Post id.</param>
        ///<returns>Partial view with ratings</returns>
        public ActionResult SetLike(int postId)
        {
            ratingService.SetLike(postId, User.Identity.GetUserId<int>());
            return GetRating(postId);
        }
        [Authorize(Roles = "Author")]
        /// <summary>
        ///Set dislike to post. 
        ///</summary>
        ///<param name="postId">Post id.</param>
        ///<returns>Partial view with ratings</returns>
        public ActionResult SetDislike(int postId)
        {
            ratingService.SetDislike(postId, User.Identity.GetUserId<int>());
            return GetRating(postId);
        }
    }
}