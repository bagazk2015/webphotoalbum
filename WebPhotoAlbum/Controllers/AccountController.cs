﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Infrastructure;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.Models.Account;

namespace WebPhotoAlbum.Controllers
{
    public class AccountController : Controller
    {
        private IAuthorService UserService { get; }
      
        private IAdminService AdminService { get; }

        private IApplicationUserService ApplicationUserService { get; }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }


    public AccountController(IAuthorService _authorService,IAdminService _adminService,IApplicationUserService _applicationUserService)
        {
            UserService = _authorService;
            AdminService = _adminService;
            ApplicationUserService = _applicationUserService;
        }
        public ActionResult Login()
        {
            return View();
        }

          private string GetControllerNameByRole(ApplicationUserDTO user)
        {
            string role = ApplicationUserService.GetUserRoleByEmail(user.Email);
            if (role =="Author")
            {
                return "AuthorProfile";
            }
            else
            {
                if (role== "Admin")
                {
                    return "AdminUsers";
                }
                else
                {
                    return "Home";
                }
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model)
        {
            await SetInitialDataAsync();
            if (ModelState.IsValid)
            {
             ApplicationUserDTO userDto = new ApplicationUserDTO { Email = model.Email, Password = model.Password };
                ClaimsIdentity claim = await UserService.Authenticate(userDto);
                if (claim == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);

                    string returnUrl = GetControllerNameByRole(userDto);
                    return RedirectToAction("Index",returnUrl );
                }
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            await SetInitialDataAsync();
            if (ModelState.IsValid)
            {
                AuthorDTO userDto = new AuthorDTO
                {
                    Email = model.Email,
                    Password = model.Password,
                    Name=model.Name,
                    Surname=model.Surname,
                    Role = "Author"
                };
                OperationDetails operationDetails = await UserService.Create(userDto);
                if (operationDetails.Succedeed)
                    return RedirectToAction("Index", "Home");
                else
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
            }
            return View(model);
        }
 
        private async Task SetInitialDataAsync()
        {
            await AdminService.SetInitialData(new AdminDTO
            {
                Email = "admin@gmail.com",
                Password = "password12345",
                Name = "Сергей",
                Role = "Admin",
                Surname = "Крюков",
                Patronymic="Олегович"
           


            }, new List<string> { "Author", "Admin" }); ;
        }
    }
}