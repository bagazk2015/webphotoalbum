﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.Models.AdminUsers;
using WebPhotoAlbum.Models.AuthorProfile;

namespace WebPhotoAlbum.Controllers
{
    [Authorize(Roles ="Admin")]
    public class AdminUsersController : Controller
    {
        private readonly IAuthorService authorService;
        private readonly IAdminService adminService;
        private readonly IMapper _mapper;
        public AdminUsersController(IAuthorService _authorService, IPostService _postService,IAdminService _adminService)
        {
            authorService = _authorService;
            adminService = _adminService;
            _mapper = new MapperConfiguration(cgf =>
            {
                cgf.CreateMap<AuthorDTO, UserAuthorVM>();

                cgf.CreateMap<AdminDTO, UserAuthorVM>();

            }).CreateMapper();
        } 
        /// <summary>
        ///Returns index view to user. 
        ///</summary>
        ///<returns>View with index page</returns>
        public ActionResult Index()
        {
            List<AuthorDTO> authorDTOs = authorService.GetAuthors();
            List<AdminDTO> adminDTOs = adminService.GetAdmins();
            List<UserAuthorVM> userAuthorVMs = _mapper.Map<List<AuthorDTO>, List<UserAuthorVM>>(authorDTOs);
            List<UserAuthorVM> userAdminVMs= _mapper.Map<List<AdminDTO>, List<UserAuthorVM>>(adminDTOs);
            UsersForAdminVM usersForAdminVM = new UsersForAdminVM();
            usersForAdminVM.Admins = userAdminVMs;
            usersForAdminVM.Authors = userAuthorVMs;
            return View(usersForAdminVM);
        }

        /// <summary>
        ///If user confirmed the deletion method delete post. 
        ///</summary>
        ///<param name="id">Post id.</param>
        ///<returns>Index view</returns>
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AuthorDTO authorDTO = authorService.GetAuthor((int)id);
            if (authorDTO== null)
            {
                return HttpNotFound();
            }
            return View(id);
        }

        /// <summary>
        ///If user confirmed the deletion method delete post. 
        ///</summary>
        ///<param name="id">Post id.</param>
        ///<returns>Index view</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            authorService.DeleteAuthor(id); 
        
            return RedirectToAction("Index");
        }
    }
}