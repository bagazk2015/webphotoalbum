﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.Models.Home;

namespace WebPhotoAlbum.Controllers
{


    public class HomeController : Controller
    {
        private readonly IPostService postService;

        public HomeController(IPostService _postService)
        {
            postService = _postService;

        }
        /// <summary>
        ///Returns view with posts. 
        ///</summary>

        ///<returns>View with posts</returns>
        public ActionResult Index()
        {
            List<PostDTO> postDTOs = postService.GetPostsNumber(Int32.Parse(ConfigurationSettings.AppSettings["PageSize"]));
            List<DisplayPostVM> postVMs = new List<DisplayPostVM>();


            foreach (PostDTO postDTO in postDTOs)
            {
                DisplayPostVM displayPostVM = new DisplayPostVM()
                {
                    Id = postDTO.Id,
                    ImageData = postDTO.ImageData,
                    ImageMimeType = postDTO.ImageMimeType,



                };

                postVMs.Add(displayPostVM);


            }
            Session["NextPosts"] = postVMs;
            return View(postVMs);
        }



    }
}