﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPhotoAlbum.BLL.DTO;
using WebPhotoAlbum.BLL.Interfaces.Services;
using WebPhotoAlbum.Models.Tag;

namespace WebPhotoAlbum.Controllers
{
    public class TagController : Controller
    {
        private ITagService tagService;

        private readonly IMapper _mapper;
        public TagController(ITagService _tagService)
        {
            tagService = _tagService;
            _mapper = new MapperConfiguration(cgf =>
            {
                cgf.CreateMap<TagDTO,TagVM>().ReverseMap();

            }).CreateMapper();
        }
      
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        ///Return partial view with modal for adding tag. 
        ///</summary>

        ///<returns>Partial view with modal for adding tag</returns>
        [HttpGet]
        public ActionResult AddTag()
        {
            return PartialView("~/Views/Tag/Partial/SearchTagModal.cshtml", new TagVM());
        }
        /// <summary>
        ///Set dislike to post. 
        ///</summary>
        ///<param name="tagVM">Added tag view model.</param>
        ///<returns>Partial view with list of tags</returns>
        [HttpPost]
        public ActionResult AddTag(TagVM tagVM)
        {  
            ICollection<TagVM> tagVMs = Session["Tags"] as ICollection<TagVM>;
           
            if (ModelState.IsValid)
            {
                tagVMs.Add(tagVM);
            } 
            Session["Tags"] = tagVMs;
            return PartialView("~/Views/Tag/Partial/NewPostTagsList.cshtml",tagVMs);
        }
        /// <summary>
        ///Remove choosed tag from post. 
        ///</summary>
        ///<param name="title">Title of tag for deleting.</param>
        ///<returns>Partial view with list of tags</returns>
        [HttpPost]
        public ActionResult RemoveTag(string title)
        {
            ICollection<TagVM> tagVMs = Session["Tags"] as ICollection<TagVM>;

            TagVM tagToRemove = tagVMs.FirstOrDefault(x => x.Title == title);

            tagVMs.Remove(tagToRemove);

            Session["Tags"] = tagVMs;

            return PartialView("~/Views/Tag/Partial/NewPostTagsList.cshtml", tagVMs);
        }
        /// <summary>
        ///Search tags by title. 
        ///</summary>
        ///<param name="searchText">Search title tag.</param>
        ///<returns>Partial view with list of searched tags</returns>
        [HttpGet]
        public ActionResult SearchTags(string searchText)
        {
          List<TagVM> tagVMs=_mapper.Map<List<TagDTO>,List<TagVM>>(tagService.SearchByTitle(searchText));
            return PartialView("~/Views/Tag/Partial/SearchedTagsList.cshtml",tagVMs);
        }
    }
}