﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPhotoAlbum.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
  
        public ViewResult Index()
        {
            return View("Error");
        }
        public ViewResult NotFound()
        {
            Response.StatusCode = 404;
            return View("NotFound");
        }
        public ViewResult Forbidden()
        {
            Response.StatusCode = 403;
            return View("Forbidden");
        }
        public ViewResult Internal()
        {
            Response.StatusCode =500;
            return View("Internal");
        }
    }
}