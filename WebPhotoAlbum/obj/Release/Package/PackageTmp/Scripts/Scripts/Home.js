﻿

function showImages() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("images").style.display = "block";
}


var PrevSearchText = "";


function SearchPost() {
    if (PrevSearchText != $("#searchPostInput").val()) {
        page = 0;
        console.log(document.getElementById("searchPostInput"));
 

            $.ajax({
                type: "POST",
                url: "/Post/GetNextPosts",
                data: { pageCount: page, searchText: $("#searchPostInput").val() },

                success: function (data) {
                    $("#imagesGrid").html(data);
                }
            });
        }
        PrevSearchText = $("#searchPostInput").val();


    }

function ShowPostModal(Id) {



    $.ajax({
        type: "GET",
        url: "/Post/GetPostDisplayToHome",
        data: { Id: Id },

        success: function (data) {
            $("#ShowPostModal").html(data);
            $("#ShowPostModal").modal('show');
        }
    });

}
