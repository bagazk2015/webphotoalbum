﻿

function AddTag() {


    const url = '/Tag/AddTag';
    title = document.getElementById('TagTitle').value;
    let item = {
       
        Title: title
    }
    
    $.ajax(url, {
        method: 'post',
        contentType: 'application/json',
        data: JSON.stringify(item),
        traditional: true,

        success: function (data, textStatus, xhr) {
            if (xhr.getResponseHeader("X-Responded-JSON") != null
                && JSON.parse(xhr.getResponseHeader("X-Responded-JSON")).status == "401") {
                window.location.href = '/Account/Login/'
            }
            let elementName = "#tags";
            $(elementName).html(data);
          
        }
    });

}
function RemoveTag(title) {


    const url = '/Tag/RemoveTag';
  
    let item = {

        Title: title
    }

    $.ajax(url, {
        method: 'post',
        contentType: 'application/json',
        data: JSON.stringify(item),
        traditional: true,

        success: function (data, textStatus, xhr) {
            if (xhr.getResponseHeader("X-Responded-JSON") != null
                && JSON.parse(xhr.getResponseHeader("X-Responded-JSON")).status == "401") {
                window.location.href = '/Account/Login/'
            }
            let elementName = "#tags";
            $(elementName).html(data);

        }
    });

}
function NewPostIsValid()
{
    if (document.getElementById("inputImage").value == "") {
        alert("You cannot create post withou image!")
    }
}
function GetNewPostModal() {


    const url = '/Post/CreatePost';
    //var url = $('#CreateRequestModal').data('url');
    $.get(url, function (data, textStatus, xhr) {
        if (xhr.getResponseHeader("X-Responded-JSON") != null
            && JSON.parse(xhr.getResponseHeader("X-Responded-JSON")).status == "401") {
            window.location.href = '/Account/Login/'
        }
        $("#CreatePostModal").html(data);
        $("#CreatePostModal").modal('show');

    })

}



function GetTagModal() {


    const url = '/Tag/AddTag';
    //var url = $('#CreateRequestModal').data('url');
    $.get(url, function (data) {
        $("#CreateTagModal").html(data);
        $("#CreateTagModal").modal('show');

    })

}

    var PrevSearchTagText = "";



    function SearchTagInterval() {
        $("#TagTitle").off('input');
        setTimeout(SearchTag, 500);

    }

    function SearchTag() {
        if (PrevSearchTagText != $("#TagTitle").val()) {
      
            if ($("#TagTitle").val() == "") {

            }
            else {
                $.ajax({
                    type: "GET",
                    url: "/Tag/SearchTags",
                    data: { searchText: $("#TagTitle").val() },

                    success: function (data, textStatus, xhr) {
                        if (xhr.getResponseHeader("X-Responded-JSON") != null
                            && JSON.parse(xhr.getResponseHeader("X-Responded-JSON")).status == "401") {
                            window.location.href = '/Account/Login/'
                        }
                        $("#searchedTags").html(data);
                    }
                });
            }
            PrevSearchTagText = $("#TagTitle").val();


        }
        $("#TagTitle").on("input", SearchTagInterval);
    }

function ChooseTag(elem) {
  
    document.getElementById("TagTitle").value = elem.value;
}



function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image')
                .attr('src', e.target.result)
            document.getElementById("inputLabel").style.padding = "0px"; 
        };

        reader.readAsDataURL(input.files[0]);
    }
}
//$('#formNewPost').submit(function () {


//    $('#CreatePostModal').modal('hide');
//    return false;
//});

