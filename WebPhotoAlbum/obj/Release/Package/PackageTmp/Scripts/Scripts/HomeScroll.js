﻿
$(function () {


    $('div#loading').hide();
    var imagesLoaded = 0;
    var page = 0;
    var _inCallback = false;
    function loadItems() {

        if (page > -1 && !_inCallback) {
            _inCallback = true;
            page++;
            $('div#loading').show();

            $.ajax({
                type: 'POST',
                url: '/Post/GetNextPosts',
                data: { pageCount: page, searchText: PrevSearchText },
                success: function (data, textstatus) {
                    if (data != '') {
                        $("#imagesGrid").append(data);
                        setTimeout(function () { _inCallback =false;  }, 2000);
                    }
                    else {
                        page = -1;

                        setTimeout(function () { _inCallback = false;  }, 2000);
                    }



                }
            });
        }
   
    }

  
    // обработка события скроллинга
    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            $("div#loading").hide();
            loadItems();
        }
    });


})

function ShowImage(sender) {

    sender.parentElement.childNodes.item("imageLoader").style.display = "none";
    sender.classList.remove("hideImg");

}